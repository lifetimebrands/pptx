module PPTX
  module Shapes
    class Picture < Shape
      attr_reader :options

      def initialize(transform, relationship_id, options={shadow: false})
        super(transform)
        @relationship_id = relationship_id
        @options = options
      end

      def base_xml
        """
        <p:pic xmlns:a='http://schemas.openxmlformats.org/drawingml/2006/main'
               xmlns:p='http://schemas.openxmlformats.org/presentationml/2006/main'>
          <p:nvPicPr>
              <p:cNvPr descr='random.jpg' id='2' name='Image 1'/>
              <p:cNvPicPr>
                  <a:picLocks noChangeAspect='1'/>
              </p:cNvPicPr>
              <p:nvPr/>
          </p:nvPicPr>
          <p:blipFill>
              <a:blip r:embed='REPLACEME'/>
              <a:stretch>
                  <a:fillRect/>
              </a:stretch>
          </p:blipFill>
          <p:spPr>
          </p:spPr>
        </p:pic>
        """
      end

      def drop_shadow_xml
        <<-XML
        <a:effectLst>
            <a:outerShdw blurRad="50800" dist="38100" dir="2700000" algn="tl" rotWithShape="0">
                <a:prstClr val="black">
                    <a:alpha val="40000"/>
                </a:prstClr>
            </a:outerShdw>
        </a:effectLst>
        XML
      end

      def build_node
        base_node.tap do |node|
          # Set the relationship ID for the image
          node.xpath('.//a:blip', a: DRAWING_NS).first['r:embed'] = @relationship_id

          if options[:shadow]
            spPr_node = node.xpath('.//p:spPr', 'p' => 'http://schemas.openxmlformats.org/presentationml/2006/main').first
            shadow_node = Nokogiri::XML::DocumentFragment.parse(drop_shadow_xml.strip)
            spPr_node.add_child(shadow_node)
          end
        end
        base_node
      end


    end
  end
end
