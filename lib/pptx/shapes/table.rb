module PPTX
    module Shapes
      class Table < Shape
        attr_reader :rows, :cols, :cell_data

        def initialize(transform,  column_widths = [], column_alignments = [], cell_data = [])
          super(transform)
          @rows = cell_data.length
          @cols = cell_data.empty? ? 0 : cell_data.first.length
          @cell_data = cell_data
          @row_height = 20 * 12700

          # Set column widths, default to equal widths if not specified
          default_width = 90 # Example default width in  12700
          @column_widths = column_widths.empty? ? Array.new(@cols, default_width) : column_widths
          # Set column alignments, default to center if not specified
          @column_alignments = column_alignments.empty? ? Array.new(@cols, "ctr") : column_alignments

        end


        def base_xml
          """
         <p:graphicFrame xmlns:a='http://schemas.openxmlformats.org/drawingml/2006/main' xmlns:p='http://schemas.openxmlformats.org/presentationml/2006/main' xmlns:r='http://schemas.openxmlformats.org/officeDocument/2006/relationships'>
            <p:nvGraphicFramePr>
              <p:cNvPr id='100' name='Table data'/>
               <p:cNvGraphicFramePr/>
              <p:nvPr/>
            </p:nvGraphicFramePr>
            <p:spPr>
            </p:spPr>
            <a:graphic>
              <a:graphicData uri='http://schemas.openxmlformats.org/drawingml/2006/table'>
                <a:tbl>
                  #{tbl_pr_xml}
                  #{tbl_grid_xml}
                  #{generate_table_content}
                </a:tbl>
              </a:graphicData>
            </a:graphic>
          </p:graphicFrame>
          """.strip
        end

        def build_node
          Nokogiri::XML::DocumentFragment.parse(base_xml)
          node = Nokogiri::XML::DocumentFragment.parse(base_xml)
          set_shape_properties_table(node, *@transform)
          node
        end


        private

        def transform_xml
          # Placeholder for transformation XML, adjust as needed.
         "<a:off x='2032000' y='719666'/><a:ext cx='8128000' cy='741680'/>"
        end


        def tbl_grid_xml
          grid_cols_xml = @column_widths.map.with_index do |width, col|
            calculated_width = (12700 * width.to_f).to_i
            <<-XML
            <a:gridCol w='#{calculated_width}'>
            </a:gridCol>
            XML
          end.join.strip

          "<a:tblGrid>#{grid_cols_xml}</a:tblGrid>".strip
        end



        def tbl_pr_xml
          "<a:tblPr firstRow='1' bandRow='1'/>"
        end

        def generate_table_content

            #border: { width: 1.5, dash_style: "dot" },


          (1..@rows).map do |row|
            "<a:tr h='#{@row_height}' >#{generate_cells_for_row(row)}</a:tr>".strip
          end.join
        end

        def generate_cells_for_row(row)
          total_rows = @rows


          (1..@cols).map do |col|
            cell_info = @cell_data[row - 1][col - 1]
            cell_content = cell_info[:content]
            cell_formatting = cell_info[:formatting]
            border_xml = generate_border_xml(row - 1, col - 1, total_rows, cell_formatting)
            typeface = cell_formatting[:typeface] || 'Arial'
            font_size = cell_formatting[:font_size] || 24.0
            bold =  cell_formatting[:bold] || 0
            font_size_emu = (font_size * 100).to_i
            alignment = @column_alignments[col - 1] || 'ctr'

            <<-CELL_XML
              <a:tc>
                <a:txBody>
                  <a:bodyPr/>
                  <a:lstStyle/>
                  <a:p>
                    <a:pPr algn='#{alignment}' rtl='0'/>
                    <a:r>
                        <a:rPr lang='en-US' sz='#{font_size_emu}' b='#{bold}' dirty='0'>
                            <a:latin typeface='#{typeface}'/>
                        </a:rPr>
                      <a:t>#{cell_content}</a:t>
                    </a:r>
                  </a:p>
                </a:txBody>
                <a:tcPr>
                #{border_xml}
                </a:tcPr>
              </a:tc>
            CELL_XML
          end.join.strip
        end

        def generate_border_xml(row_index, col_index, total_rows, formatting)
          border = formatting[:border] || {}
          width_in_pt = border[:width] || 1.0 # Assuming a default border width
          dash_style = border[:dash_style] || "solid"
          color = border[:color] || "000000"
          background_color = formatting[:background_color] || nil
          border_placement = formatting[:placement] || "all"  # all, outside, inside, outside_row, outside_col
          width_in_emus = (width_in_pt * 12700).to_i
          total_cols = @cols

          background_color_xml = background_color == nil ? "<a:noFill/>" : "<a:solidFill><a:srgbClr val='#{background_color}'/></a:solidFill>"

          # Initialize the sides with borders for all cells
          case border_placement
            when "outside"
              sides = %w[]
              sides << 'T' if row_index == 0
              sides << 'B' if row_index == total_rows - 1
              sides << 'L' if col_index == 0
              sides << 'R' if col_index == total_cols - 1
            when "all"
              sides = %w[L R T B]
            when "inside"
              sides = %w[]
              sides << 'B' unless row_index == total_rows - 1
              sides << 'R' unless col_index == total_cols - 1
            when "inside_row"
              sides = %w[]
              sides << 'B' unless row_index == total_rows - 1
            when "outside_row"
              sides = %w[T B]
              sides << 'L' if col_index == 0
              sides << 'R' if col_index == total_cols - 1
            when "outside_col"
              sides = %w[L R]
              sides << 'T' if row_index == 0
              sides << 'B' if row_index == total_rows - 1
            when "l"
              sides = %w[L]
            when "r"
              sides = %w[R]
            when "t"
              sides = %w[T]
            when "b"
              sides = %w[B]
            end



          # Generate border XML, applying <a:noFill/> for sides without borders
          sides_xml = %w[L R T B].map do |side|
            if sides.include?(side)
              <<-BORDER_XML
                <a:ln#{side} w='#{width_in_emus}' cap='flat' cmpd='sng' algn='ctr'>
                  <a:solidFill><a:srgbClr val='#{color}'/></a:solidFill>
                  <a:prstDash val='#{dash_style}'/>
                  <a:round/>
                  <a:headEnd type='none' w='med' len='med'/>
                  <a:tailEnd type='none' w='med' len='med'/>
                </a:ln#{side}>
              BORDER_XML
            else
              "<a:ln#{side}>#{background_color_xml}</a:ln#{side}>"
            end
          end.join

          sides_xml
        end




      end
    end
  end
